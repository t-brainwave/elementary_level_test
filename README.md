## 初級者用WEBアプリケーション開発テスト

環境要件
--------

下記の環境要件を満たすこと

- `言語`：PHP5.X
- `フレームワーク`：CakePHP2.X
- `データベース`：MySQL5.X

基本要件
--------

ユーザーを管理する基本機能開発  
基本機能は以下の5つとする

- 一覧
- 新規追加
- 編集
- 詳細
- 削除

詳細要件
--------

#### 全体

- ユーザーのキーはIDとする
- どのページからでも一覧画面に遷移できるリンクボタンを設置をすること
- history.back()機能は使用禁止とする
- リンク及びフォーム等のタグ生成にはヘルパーを使用すること

#### 一覧

- ID順にソート表示とする
- 1ページ3件表示でページング処理すること

|ID|名前|メールアドレス|登録日||
|:-:|:-|:-|:-:|:-:|
|1|tanaka|a@hoge.com|2015/12/31|[詳細] [編集] [削除]|
|2|sato|b@hoge.com|2015/01/01|[詳細] [編集] [削除]|
|3|suzuki|c@hoge.com|2015/04/15|[詳細] [編集] [削除]|

[前へ] [次へ]

※[]はリンクボタンとする

#### 新規追加

- 登録処理前の確認画面機能を必ず設けること（入力→確認→完了）
- 追加処理後は詳細画面にリダイレクトすること
- バリデーション処理、エラー表示をすること

#### 編集

- 登録処理前の確認画面機能を必ず設けること（入力→確認→完了）
- 変更処理後は詳細画面にリダイレクトすること
- バリデーション処理、エラー表示をすること

#### 詳細

- パスワード以外全て表示すること
- 日付はYYYY/MM/DD形式で表示すること

#### 削除

- 削除間違い防止のため確認画面を設けること
- 削除処理は物理削除とする

#### 特記事項

- `パスワード`：SHA256で暗号化すること。8字～32字、使用可能文字（アルファベット、数字、ハイフン、アンダースコア）の制限を設けること
- `メールアドレス`：メールアドレス形式をチェックすること。システムとして一意を担保すること

テーブル設計
--------

|項目|名称|必須|型|サイズ|主キー|備考|
|:-|:-|:-:|:-:|:-:|:-:|:-|
|ユーザーID|id|◯|integer|11|◯|auto_increment|
|パスワード|password|◯|varchar|255|||
|名前|name|◯|varchar|50|||
|性別|gender||integer|11||1:男、2:女、3:その他|
|生年月日|birthday||date|||YYYY-MM-DD|
|電話番号|phone||varchar|20|||
|メールアドレス|email|◯|varchar|255|||
|登録日時|created||datetime|||YYYY-MM-DD HH:MM:SS|
|更新日時|updated||datetime|||YYYY-MM-DD HH:MM:SS|


DDL及び初期データ
--------

```
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `password` varchar(255) NOT NULL,
  `name` varchar(50) NOT NULL,
  `gender` int(11) DEFAULT NULL,
  `birthday` date DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `users` (`id`,`password`,`name`,`gender`,`birthday`,`phone`,`email`,`created`,`updated`) VALUES
(1,'ecb666d778725ec97307044d642bf4d160aabb76f56c0069c71ea25b1e926825','tanaka',1,'1960-01-01','090-1111-2222','a@hoge.com',NOW(),NOW()),
(2,'ecb666d778725ec97307044d642bf4d160aabb76f56c0069c71ea25b1e926825','sato',2,'1972-12-25','090-2222-3333','b@hoge.com',NOW(),NOW()),
(3,'ecb666d778725ec97307044d642bf4d160aabb76f56c0069c71ea25b1e926825','suzuki',2,'1981-05-17','090-3333-4444','c@hoge.com',NOW(),NOW()),
(4,'ecb666d778725ec97307044d642bf4d160aabb76f56c0069c71ea25b1e926825','yamada',1,'1987-09-03','090-4444-5555','d@hoge.com',NOW(),NOW()),
(5,'ecb666d778725ec97307044d642bf4d160aabb76f56c0069c71ea25b1e926825','koike',2,'1994-03-31','090-5555-6666','e@hoge.com',NOW(),NOW());
```
